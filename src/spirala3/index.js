const express = require('express');
const bodyParser = require('body-parser');
const db = require('./util/db.js');
const app = express()
const port = process.env.PORT_SERVER ? parseInt(process.env.PORT_SERVER) : 3000

app.use(express.json())
app.use(bodyParser.text())

app.use('/',express.static('./public/html'));
app.use([/^\/public\/html($|\/)/, '/'], express.static('./public'));

app.use(express.static(__dirname+'/public/html/'));
app.use(express.static(__dirname+'/public/css/'));
app.use(express.static(__dirname+'/public/js/'));

app.get('/vjezbe', async function (req, res) {


  console.log(`Get /vjezbe ${JSON.stringify(req.body)}`)
    
    let vjezbe = await db.vjezba.findAll();
    let brojVjezbi = vjezbe.length;
    //let zadaci = await db.zadatak.findAll();
    let brojZadataka = [];

    for(const vjezba of vjezbe){
      let zadaciUVjezbi = await db.zadatak.count({where:{'vjezbaId': vjezba.dataValues.id}});
      brojZadataka.push(zadaciUVjezbi);

    }

    res.send({'brojVjezbi': Number(brojVjezbi), 'brojZadataka': brojZadataka})

  })

  app.post('/vjezbe', async function (req, res) {
    console.log(`Post /vjezbe ${JSON.stringify(req.body)}`)
    // console.log((req))
    console.log(req.body)
    if(req.body.brojVjezbi > 15 || req.body.brojVjezbi < 1) res.send({'status':'error', 'data': 'Pogresan parametar brojVjezbi'})
    let brojZadatakaNiz = [];
    brojZadatakaNiz = req.body.brojZadataka;
    console.log(brojZadatakaNiz)

    if(req.body.brojVjezbi != brojZadatakaNiz.length) res.send({'status':'error', 'data': 'Pogresan parametar brojZadataka'})

    for(let i = 0; i < brojZadatakaNiz.length; i++){
        if(req.body.brojZadataka[i] > 10 || req.body.brojZadataka[i] < 0) res.send({'status':'error', 'data': 'Pogresan parametar ' + i})
    }

    console.log("Provjera zavrsena")

    let brojVjezbi = req.body.brojVjezbi;
    
    console.log("broj zadataka niz\n", brojZadatakaNiz);
    let brojZadataka = brojZadatakaNiz.join('\n');

    // console.log(brojZadataka);
    let izlazRed = [];

    brojZadatakaNiz.forEach(zadatak =>{
      izlazRed.push({brojZadataka:zadatak});
    })

    console.log("izlazni red koji ide u objekat", izlazRed)

    let returnObject = {'brojVjezbi': Number(brojVjezbi), 'brojZadataka': brojZadatakaNiz}

    /*let staVraca = await db.vjezba.destroy({truncate: true});
    let rez = await db.vjezba.findAll();
    await db.vjezba.bulkCreate(izlazRed);*/

    await db.zadatak.destroy({truncate: { cascade: true }});
    let staVraca = await db.vjezba.destroy({truncate: { cascade: true }});

    let vjezbe = [];
    for(let i = 0; i < brojVjezbi; i++){
      vjezbe.push({'naziv': "Vjezba" + Number(i+1)});
    }

    let tabelaVjezbe = await db.vjezba.bulkCreate(vjezbe);
    let zadaciVjezbe = [];

    let objekat = {'brojVjezbi' : 4, 'brojZadataka' : [1,2,3,4]};
    for(let i = 0; i < brojZadatakaNiz.length; i++){
      for(let j = 0; j < brojZadatakaNiz[i]; j++){
        await tabelaVjezbe[i].createZadaciVjezbe({naziv:"z" + Number(j+1)});
      }
    }

    let tabelaZadaci = await db.zadatak.findAll();
    
    res.send(returnObject)
  })

  app.post('/student', async function (req, res) {
    console.log(`Post /student ${JSON.stringify(req.body)}`)
    
    let imeString = req.body.ime;
    let prezimeString = req.body.prezime;
    let indexString = req.body.index;
    let grupaString = req.body.grupa;

    if(imeString.trim()==="" || prezimeString.trim()==="" || indexString.trim()==="" || grupaString.trim()==="" || Number(indexString)<1){
      res.send({status: "Svi podaci moraju biti popunjeni"});
      return;
    }
    // pretraziti studenta po indexu 
    let postojeciStudent = await db.student.findOne({where: {index:indexString}});
    if(postojeciStudent !== null){
      res.send({status: "Student sa indexom " + indexString + " vec postoji!"});
      return;
    }
    //provjera grupe i dodavanje studenta
    let postojecaGrupa = await db.grupa.findOne({where:{naziv:grupaString}})
    
    if(postojecaGrupa === null){
      // await db.sequelize.sync({force:true})
      postojecaGrupa = await db.grupa.create({naziv:grupaString});
    }

    await postojecaGrupa.createStudentiGrupe({ime:imeString, prezime: prezimeString, index:indexString})

    res.send({status: "Kreiran student!"})
    // dodati studenta u grupu 

  })

  app.put('/student/:index', async function (req, res) {
    
    console.log(`Put /student/${req.params['index']} ${JSON.stringify(req.body)}`);

    let indexStudenta = req.params['index'];
    let novaGrupaString = req.body.grupa;
    if(indexStudenta.trim() ==="" || novaGrupaString.trim() ===""){
      res.send({status: "sve polja moraju biti popunjena"});
      return;
    }
    //provjera da li postoji student sa indeks
    let postojeciStudent = await db.student.findOne({where:{index:indexStudenta}});
    // console.log(postojeciStudent);
    if(postojeciStudent === null){
      res.send({status: "Student sa indexom " + indexStudenta + " ne postoji"});
      return;
    }
    let novaGrupa = await db.grupa.findOne({where:{naziv:novaGrupaString}})
    if(novaGrupa === null){
      novaGrupa = await db.grupa.create({naziv:novaGrupaString});
    }
    
    let noviID = novaGrupa.id; // vidjet cemo 
    await postojeciStudent.update({grupaId:noviID})
    res.send({status: "Promjenjena grupa studentu " + indexStudenta});
  })

  app.post('/batch/student', async function (req, res) {
    console.log(`Post /batch/student ${JSON.stringify(req.body)}`)
    
    let csvDatoteka = req.body;
    let csvArray = csvDatoteka.split('\n');
    let studentiArray = [];
    csvArray.forEach(red => {
      if(red){
        const student = red.split(',');
        studentiArray.push({ime:student[0], prezime:student[1], index:student[2], grupa:(student[3]).trim()});
      }
    })
    let upisaniStudentiIndex = [];

    for (const student of studentiArray){

      let postojeciIndex = student.index;
      let postojeciStudent = await db.student.findOne({where:{index:postojeciIndex}});
      if(postojeciStudent === null){
        let postojecaGrupa = await db.grupa.findOne({where:{naziv:student.grupa}});
        if(postojecaGrupa === null){
          postojecaGrupa = await db.grupa.create({naziv:student.grupa});
        }
        //kreirati grupu
        //postojecaGrupa.createStudentiGrupe(student);
        await postojecaGrupa.createStudentiGrupe({ime:student.ime, prezime:student.prezime, index:student.index, grupa:student.grupa});
        console.log("Novi student dodan");
      }
      else{
        console.log("Student sa indeksom " + student.index + " vec postoji!");
        let pomocna = student.index;
        upisaniStudentiIndex.push(pomocna);
        console.log(upisaniStudentiIndex.length)
      }
    }

    if(upisaniStudentiIndex.length === 0) res.send({status: "Dodano " + studentiArray.length + " studenata!"});
    else res.send({status: "Dodano " + (studentiArray.length - upisaniStudentiIndex.length) + " studenata, a studenti " + upisaniStudentiIndex + " vec postoje!"});


  })

db.sequelize.sync().then(() => app.listen(port));

module.exports = {db, server: app}
