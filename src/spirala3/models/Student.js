const Sequelize = require("sequelize");

module.exports = function(sequelize,DataTypes){
    const Student = sequelize.define("student",{
        ime:{
            type: Sequelize.STRING,
            allowNull: false
        },
        prezime:{
            type:Sequelize.STRING,
            allowNull: false
        },
        index:{
            type:Sequelize.STRING,
            allowNull: false
        }
       
    })
    return Student;
};