var StudentAjax = (function(){
    let dodajStudenta = function(student,fnCallback){

        let ajax = new XMLHttpRequest();
        const URL = "http://localhost:3000/student";
        ajax.onreadystatechange = function() {
            console.log(ajax.readyState, ajax.status)
            if (ajax.readyState == 4 && ajax.status == 200) // uspjesno
                fnCallback(null, ajax.responseText)
            if (ajax.readyState == 4 && ajax.status == 404) // neuspjesno
            fnCallback(ajax.responseText ,null)
        }
        ajax.open("POST", URL, true);
        ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        ajax.send(JSON.stringify(student));

        console.log(ajax.responseText)
    }

    let postaviGrupu = function(index,grupa,fnCallback){

        let ajax = new XMLHttpRequest();
        const URL = `http://localhost:3000/student/${index}`;
        ajax.onreadystatechange = function() {
            console.log(ajax.readyState, ajax.status)
            if (ajax.readyState == 4 && ajax.status == 200) // uspjesno
                fnCallback(null, ajax.responseText)
            if (ajax.readyState == 4 && ajax.status == 404) // neuspjesno
            fnCallback(ajax.responseText ,null)
        }
        ajax.open("PUT", URL, true);
        ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
        //ajax.send({grupa});
        ajax.send(JSON.stringify({'grupa':grupa}));

        
    }

    let dodajBatch = function(csvStudenti,fnCallback){

        let ajax = new XMLHttpRequest();
        const URL = `http://localhost:3000/batch/student`;
        ajax.onreadystatechange = function() {
            console.log(ajax.readyState, ajax.status)
            if (ajax.readyState == 4 && ajax.status == 200) // uspjesno
                fnCallback(null, ajax.responseText)
            if (ajax.readyState == 4 && ajax.status == 404) // neuspjesno
            fnCallback(ajax.responseText ,null)
        }
        ajax.open("POST", URL, true);
        ajax.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
        //ajax.send({grupa});
        ajax.send(csvStudenti);
    }

    return{
        dodajStudenta:dodajStudenta,
        postaviGrupu:postaviGrupu,
        dodajBatch:dodajBatch
    }

}());