var VjezbeAjax = (function(){
    let dodajInputPolja = function(DOMelementDIVauFormi,brojVjezbi){
        //clean uo
        if(DOMelementDIVauFormi.hasChildNodes()){
            while(DOMelementDIVauFormi.firstChild){
                DOMelementDIVauFormi.removeChild(DOMelementDIVauFormi.firstChild)
            }
        }
        //provjera
        if(brojVjezbi < 1 || brojVjezbi > 15) return
        let form = document.createElement("form")
        for(let i = 0; i < brojVjezbi; i++){
            let label = document.createElement("label")
            label.setAttribute("for", "vjezba" + Number(i+1))
            label.innerHTML = "Broj zadataka na vjezbi " + Number(i+1) +" ";
            form.appendChild(label)
        
            let input = document.createElement("input")
            input.setAttribute("type", "number")
            input.setAttribute("id", "z" +i)
            input.setAttribute("name", "z" +i)
            input.setAttribute("value", "4")
            input.setAttribute("style", "width:100%")
            form.appendChild(input)
        }
        DOMelementDIVauFormi.appendChild(form)
        // let button = document.createElement("button")
        // button.setAttribute("type", "submit")
        // button.setAttribute("id", "btnPosalji")
        // button.innerHTML = "Posalji"
        // DOMelementDIVauFormi.appendChild(button)
    }

    let posaljiPodatke = function(vjezbeObjekat,callbackFja){
        //ovdje provjeriti unos prvo
        try{
            let greska = "";
            if(vjezbeObjekat.brojVjezbi < 1 || vjezbeObjekat.brojVjezbi > 15) greska += "Pogresan parametar brojVjezbi "
            //let pom = vjezbeObjekat.brojZadataka.length // ovo ne radi iz nekog razloga
            if(vjezbeObjekat.brojVjezbi !== vjezbeObjekat.brojZadataka.length) greska += "Pogresan parametar brojZadataka "

            for(let i=0; i<vjezbeObjekat.brojZadataka.length; i++){
                if(vjezbeObjekat.brojZadataka[i]<0 || vjezbeObjekat.brojZadataka[i]>10){
                    greska = "Pogresan parametar z";
                    break;
                } 
            }

            if(greska != ""){
                callbackFja(greska,null);
                console.log(greska);
                return; // treba li ovdje return???
            }
            let ajax = new XMLHttpRequest();
            const URL = "http://localhost:3000/vjezbe";
            ajax.onreadystatechange = function() {
                console.log(ajax.readyState, ajax.status)
                if (ajax.readyState == 4 && ajax.status == 200) // uspjesno
                    callbackFja(null, ajax.responseText)
                if (ajax.readyState == 4 && ajax.status == 404) // neuspjesno
                    callbackFja(ajax.responseText ,null)
            }
            ajax.open("POST", URL, true);
            ajax.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
            ajax.send(JSON.stringify(vjezbeObjekat), callbackFja);
        }
        catch(e){
            callbackFja("error",null);
            console.log("error");
        }

    }

    let dohvatiPodatke = function(callbackFja) {
        var ajax = new XMLHttpRequest();
        ajax.onreadystatechange = function() {
            if (ajax.readyState == 4 && ajax.status == 200){
                callbackFja(null, JSON.parse(ajax.response));
            }   
            else if(ajax.readyState == 4 && ajax.status == 404){
                callbackFja("Nije uspjelo",null);
            }
            console.log("ajax.readyState :" + ajax.readyState + " ajax.status :" + ajax.status)
        }

        
        ajax.open("GET","http://localhost:3000/vjezbe/",true);
        ajax.send();
    }

  

    let iscrtajVjezbe = function(divDOMelement, vjezbeObjekat){

        if(Number(vjezbeObjekat.brojVjezbi) < 0 || Number(vjezbeObjekat.brojVjezbi) > 15){
            console.log("Neispravan broj vjezbi je unesen");
            return;
        }
        if(Number(vjezbeObjekat.brojVjezbi) != vjezbeObjekat.brojZadataka.length){ 
            console.log("Broj vjezbi i duzina niza broj zadataka je nepodudarna");
            return;
        }

        for(let k = 0; k < vjezbeObjekat.brojVjezbi; k++){
            if(isNaN(vjezbeObjekat.brojZadataka[k]) || vjezbeObjekat.brojZadataka[k] < 0 || vjezbeObjekat.brojZadataka[k] > 10) return
        }
       

        let nizDivovaZadataka = [];
        for(let i = 0; i < vjezbeObjekat.brojVjezbi; i++){
            let btnVjezba = document.createElement("button");
            btnVjezba.className="vjezba";
            btnVjezba.innerHTML = "Vjezba " + Number(i + 1);
            btnVjezba.setAttribute("id", "btnVjezba" + Number(i+1))

            let divZaZadatke = document.createElement("div");
            divZaZadatke.className="dugmad"
            divZaZadatke.setAttribute("id", "divZaZadatke" + Number(i+1));

            nizDivovaZadataka.push(divZaZadatke);

            if(vjezbeObjekat.brojZadataka[i] != 0){
                //btnVjezba.setAttribute("onclick", "iscrtajZadatke(divZaZadatke, vjezbeObjekat.brojZadataka[i]);"); ovo ne radi
                btnVjezba.onclick = function(){
                    // if(divZaZadatke.hasChildNodes() && divZaZadatke.style.display === "flex"){
                    //     divZaZadatke.style.display = "none";
                    // }
                    // else if(divZaZadatke.hasChildNodes() && divZaZadatke.style.display === "none"){
                    //     divZaZadatke.style.display = "flex";
                    // }
                    for(let j = 0; j < nizDivovaZadataka.length; j++){
                        if(nizDivovaZadataka[j] != divZaZadatke) nizDivovaZadataka[j].style.display = "none"; //ovo sakriva ostale divove

                    }
                    if(divZaZadatke.hasChildNodes() && divZaZadatke.style.display === "none"){
                        divZaZadatke.style.display = "flex";
                    }

                    if(divZaZadatke.hasChildNodes && divZaZadatke.childElementCount != vjezbeObjekat.brojZadataka[i]){
                        while(divZaZadatke.firstChild){
                            divZaZadatke.removeChild(divZaZadatke.firstChild);
                        }
                    }
                    if(!divZaZadatke.hasChildNodes()){
                        iscrtajZadatke(divZaZadatke, vjezbeObjekat.brojZadataka[i]); //ovako na netu pise
                        divZaZadatke.style.display = "flex";
                    }


                    
                }
            }
           

            divDOMelement.appendChild(btnVjezba)
            divDOMelement.appendChild(divZaZadatke)
        }
    }

    let iscrtajZadatke = function(vjezbaDOMelement,brojZadataka){
        if(brojZadataka < 0 || brojZadataka > 10 || isNaN(brojZadataka)) return

        for(let i = 0; i < brojZadataka; i++){
            let btnZadatak = document.createElement("button");
            btnZadatak.className = "dugme";
            btnZadatak.innerHTML = "Zadatak" + Number(i+1);
            vjezbaDOMelement.appendChild(btnZadatak);
        }
    }

    return{
        dodajInputPolja:dodajInputPolja,
        posaljiPodatke:posaljiPodatke,
        dohvatiPodatke:dohvatiPodatke,
        iscrtajVjezbe:iscrtajVjezbe,
        iscrtajZadatke:iscrtajZadatke
    }
}());