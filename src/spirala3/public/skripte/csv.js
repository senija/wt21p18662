window.onload = function(){
    let btnPosalji = document.getElementById("btnPosalji");

    btnPosalji.addEventListener("click", event=>{
        
        let csvTekst = (document.getElementById("csvTekst").value).trim();

        StudentAjax.dodajBatch(csvTekst, function(err, data){
            if(err === null){
                document.getElementById("ajaxstatus").innerHTML = (JSON.parse(data)).status;
            }
        });

    })
}