window.onload = function(){
    let btnPosalji = document.getElementById("btnPosalji");

    btnPosalji.addEventListener("click", event=>{
        
        let index = (document.getElementById("index").value).trim();
        let grupa = (document.getElementById("grupa").value).trim();
        /*let index = document.getElementById("index").value;
        let grupa = document.getElementById("grupa").value;*/

        if(index === null || grupa === null || index ==="" || grupa ===""){
            document.getElementById("ajaxstatus").innerHTML = "Sva polja moraju biti popunjena";
            return;
        } 

        StudentAjax.postaviGrupu(index, grupa, function(err, data){
            if(err === null){
                document.getElementById("ajaxstatus").innerHTML = (JSON.parse(data)).status;
            }
        });

    })
}