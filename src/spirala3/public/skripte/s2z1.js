var TestoviParser =(function(){
    let dajTacnost = function(jsonString){
        try{
            let obj = JSON.parse(jsonString);
            //treba vracati procenat tacnosti zaokruzen na jednu decimalu, ako je cijeli broj prikazuje se bez decimala
            const brojTestova = obj.stats.tests;
            const brojIspravnihTestova = obj.stats.passes;
            let tacnosti = (function(){
                if(Number.isInteger(brojIspravnihTestova/brojTestova * 100)) return brojIspravnihTestova/brojTestova * 100;
                else return (brojIspravnihTestova/brojTestova * 100).toFixed(1);
            }());
            const procenatTacnosti = tacnosti + "%";
            const greske = [];
            const pomocna = obj.failures;
            if(pomocna && pomocna.length)
                for(let k of pomocna){
                    greske.push(k.fullTitle);
                }
            return{
                tacnost: procenatTacnosti,
                greske: greske
            }
        }
        catch(e){
            return{
                tacnost:"0%",
                greske: ["JSON string nije validan"]
            }
        }
    }


    let jesuLiIdenticniTestovi = function(testovi1, testovi2){
        if(testovi1.length !== testovi2.length) return false;
        let isti;
        for(let i = 0; i <testovi1.length; i++){
            isti = 0;
            for(let j = 0; j < testovi2.length; j++){
                if(testovi1[i].fullTitle == testovi2[j].fullTitle){
                    isti = 1;
                }
            }
            if(isti == 0){
                return false;
            }
        }
        return true;
    }

    let vratiBrojTestova = function(obj1, obj2){ //(broj testova koji padaju u rezultatu1 a ne pojavljuju se u rezultatu2 

        let padaju1 = obj1.failures;
        let testovi2 = obj2.tests;
        let brojTestovaKojiPadajuURez1PresjekTestovi2 = 0;
        for(let i = 0; i < padaju1.length; i++){
            for(let j = 0; j < testovi2.length; j++)
            if(padaju1[i].fullTitle === testovi2[j].fullTitle) brojTestovaKojiPadajuURez1PresjekTestovi2++;
        }
        return Number(padaju1.length) - Number(brojTestovaKojiPadajuURez1PresjekTestovi2);

    }

    let porediRezultate = function(rezultat1, rezultat2){
        try{
            let obj1 = JSON.parse(rezultat1);
            let obj2 = JSON.parse(rezultat2);


            let promjena;
            let greske = [];
            //a
            if(jesuLiIdenticniTestovi(obj1.tests, obj2.tests)){
                promjena = (dajTacnost(rezultat2)).tacnost;
                greske = dajTacnost(rezultat2).greske;
                greske.sort();
            }
            else{
                let brojTestova = vratiBrojTestova(obj1, obj2);
                promjena = (function(){
                    let pomocna = (brojTestova + obj2.failures.length)/(brojTestova+ obj2.tests.length)*100
                    if(Number.isInteger(pomocna)) return pomocna + "%";
                    else return pomocna.toFixed(1) + "%";
                }());

                let nalaziSe = 0;
                let greske1 = obj1.failures;
                let greske2 = obj2.failures;
                
                for(let i of greske1){
                    nalaziSe = 0;
                    for(let j of obj2.tests){
                        if(i.fullTitle == j.fullTitle){
                            nalaziSe = 1;
                            break;
                        }
                        
                    }
                    if(nalaziSe == 0) greske.push(i.fullTitle);
                }

                greske.sort();

                let tmp = [];

                for(let i of greske2){
                    tmp.push(i.fullTitle);
                }

                tmp.sort();

                greske = greske.concat(tmp);


            }

            

            // if(jesuLiIdenticniTestovi(obj1.tests, obj2.tests)){
            //     greske = dajTacnost(rezultat2).greske;
            //     greske.sort;
            // }
            // else
            // {
            //     let nalaziSe;
            //     for(let i =0; i <obj1.failures.length; i++){
            //         nalaziSe = 0;
            //         for(let j = 0; j <obj2.tests.length; j++){
            //             if(obj1.failures[i].fullTitle === obj2.tests[j].fullTitle) nalaziSe = 1;
            //         }

            //         if(nalaziSe === 0) greske.push(obj1.failures[i].fullTitle);
            //     }

            //     greske.sort;
            //     let greske2 = (obj2.failures).sort;
            //     greske.concat(greske2);
                
            // }

            return{
                promjena:promjena,
                greske:greske
            }


        }
        catch(e){
            return{
                promjena:"0%",
                greske:["Testovi se ne mogu izvršiti"]
            }
        }

    }

    return{
        dajTacnost:dajTacnost,
        porediRezultate: porediRezultate
    }
}());