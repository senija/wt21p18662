window.onload = function(){
    let btnPosalji = document.getElementById("btnPosalji");

    btnPosalji.addEventListener("click", event=>{
        let ime = (document.getElementById("ime").value).trim();
        let prezime = (document.getElementById("prezime").value).trim();
        let index = (document.getElementById("index").value).trim();
        let grupa = (document.getElementById("grupa").value).trim();

        StudentAjax.dodajStudenta({ime:ime, prezime:prezime, index:index, grupa:grupa}, function(err, data){
            if(err === null){
                document.getElementById("ajaxstatus").innerHTML = (JSON.parse(data)).status;
            }
        });

    })
}