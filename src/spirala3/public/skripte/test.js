let assert = chai.assert;
describe('TestoviParser', function() {
    describe('dajTacnost()', function() {
      it('Tacnost 100%', function() {
       let jsonString = `{
        "stats": {
            "suites": 2,
            "tests": 2,
            "passes": 2,
            "pending": 0,
            "failures": 0,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9
        },
        "tests": [
            {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
        ],
        "pending": [],
        "failures": [],
        "passes": [
            {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
        ]
        }` 
        let actual = TestoviParser.dajTacnost(jsonString);
        let expected = {tacnost: "100%", greske: []}; 
        assert.equal(JSON.stringify(expected), JSON.stringify(actual));
        
      });

      it('Tacnost 50%', function(){
        let jsonString = `{
            "stats": {
                "suites": 2,
                "tests": 4,
                "passes": 2,
                "pending": 0,
                "failures": 2,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 4 pada",
                    "fullTitle": "Test 4 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }

            ],
            "pending": [],
            "failures": [
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 4 pada",
                    "fullTitle": "Test 4 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "passes": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ]
            }` 
            let actual = TestoviParser.dajTacnost(jsonString);
            let expected = {tacnost: "50%", greske: ['Test 3 pada', 'Test 4 pada']}; 
            //console.log('test2', actual, expected);
            assert.equal(JSON.stringify(expected), JSON.stringify(actual));
      });

      it('Neispravan JSON', function(){
        let jsonString = `{
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 4 pada",
                    "fullTitle": "Test 4 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }

            ],
            "pending": [],
            "failures": [
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 4 pada",
                    "fullTitle": "Test 4 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "passes": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ]
            }` 
            let actual = TestoviParser.dajTacnost(jsonString);
            let expected = {tacnost: '0%', greske: ["JSON string nije validan"]}; 
            console.log('test3', actual, expected);
            assert.equal(JSON.stringify(expected), JSON.stringify(actual));
      });

      it('Tacnost 33.3%', function(){
        let jsonString = `{
            "stats": {
                "suites": 2,
                "tests": 3,
                "passes": 1,
                "pending": 0,
                "failures": 2,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 2 pada",
                    "fullTitle": "Test 2 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }

            ],
            "pending": [],
            "failures": [
                {
                    "title": "Test 2 pada",
                    "fullTitle": "Test 2 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "passes": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ]
            }` 
            let actual = TestoviParser.dajTacnost(jsonString);
            let expected = {tacnost: '33.3%', greske: ['Test 2 pada', 'Test 3 pada']}; 
            console.log('test3', actual, expected);
            assert.equal(JSON.stringify(expected), JSON.stringify(actual));
      });

      it('Sve pada', function(){
        let jsonString = `{
            "stats": {
                "suites": 2,
                "tests": 3,
                "passes": 0,
                "pending": 0,
                "failures": 3,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 2 pada",
                    "fullTitle": "Test 2 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }

            ],
            "pending": [],
            "failures": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 2 pada",
                    "fullTitle": "Test 2 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "Test 3 pada",
                    "fullTitle": "Test 3 pada",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "passes": []
            }` 
            let actual = TestoviParser.dajTacnost(jsonString);
            let expected = {tacnost: '0%', greske: ['Tabela crtaj() should draw 3 rows when parameter are 2,3','Test 2 pada', 'Test 3 pada']}; 
            console.log('test3', actual, expected);
            assert.equal(JSON.stringify(expected), JSON.stringify(actual));
      });




    });
});
   