let assert = chai.assert;
describe('TestoviParser', function() {
    chai.should();
    describe('posalji Podatke()', function() {
        beforeEach(function () {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function (xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function () {
            this.xhr.restore();
        });

        it('posaljiPodatke() sve ok', function (done) {
            let objekat = {"brojVjezbi": 3, "brojZadataka": [1,2,3] };
            let stringJson = JSON.stringify(objekat);
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, null, '');
                assert.equal(data, stringJson);
                done();
            });
            this.requests[0].requestBody.should.equal(stringJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, stringJson);
        });  
        it('posaljiPodatke() sve ok', function (done) {
            let objekat = {"brojVjezbi": 3, "brojZadataka": [1,2,3] };
            let stringJson = JSON.stringify(objekat);
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, null, '');
                assert.equal(data, stringJson);
                done();
            });
            this.requests[0].requestBody.should.equal(stringJson);
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, stringJson);
        });  

        it('posaljiPodatke() nepravilan JSON', function (done) {
            let objekat = {};
            //let stringJson = JSON.stringify(objekat);
            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.equal(err, "error", '');
                assert.equal(data, null);
                done();
            });
            this.requests[0].respond(404, { 'Content-Type': 'text/json' }, '{"data": "error"}');
        });  
        it('posaljiPodatke() nepravilni podaci brojZadataka', function (done) {
            let objekat = {"brojVjezbi": 3, "brojZadataka": [1,2,3,4] };
            let er = {"status": "error", data: "Pogresan parametar brojZadataka" }

            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(er));
        });  

        it('posaljiPodatke() nepravilni podaci brojVjezbi', function (done) {
            let objekat = {"brojVjezbi": 16, "brojZadataka": [1,2,3,4] };
            let er = {"status": "error", data: "Pogresan parametar brojVjezbi Pogresan parametar brojZadataka " }

            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(er));
        });  

        it('posaljiPodatke() nepravilni podaci brojVjezbi niz', function (done) {
            let objekat = {"brojVjezbi": 4, "brojZadataka": [-1,2,3,4] };
            let er = {"status": "error", data: "Pogresan parametar z" }

            VjezbeAjax.posaljiPodatke(objekat, function (err, data) {
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].requestBody.should.equal(JSON.stringify(objekat));
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify(er));
        });
    });

    describe('dohvatiPodatke()', function () {
        beforeEach(function () {
            this.xhr = sinon.useFakeXMLHttpRequest();

            this.requests = [];
            this.xhr.onCreate = function (xhr) {
                this.requests.push(xhr);
            }.bind(this);
        });

        afterEach(function () {
            this.xhr.restore();
        });
        it('Neuspjesno', function (done) {
            let error = {"status": 404, "data": "Nije uspjelo" };
            let errorString = JSON.stringify(error);
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, "Nije uspjelo", '');
                assert.deepEqual(data, null);
                done();
            });
            this.requests[0].respond(404, { 'Content-Type': 'text/json' }, errorString);
        });
        it('Ispravni podaci', function (done) {
            VjezbeAjax.dohvatiPodatke(function (err, data) {
                assert.equal(err, null, '');
                assert.deepEqual(data, {"brojVjezbi": 2, "brojZadataka": [2, 2]});
                done();
            });
            this.requests[0].respond(200, { 'Content-Type': 'text/json' }, JSON.stringify({"brojVjezbi": 2, "brojZadataka": [2, 2] }));
        });


    });

    describe('dodajInputPolja(), iscrtajVjezbe(), iscrtajZadatke()', function () {
      it('test dodajInputPolja() 2', function() {
        var div = document.createElement("div")
        VjezbeAjax.dodajInputPolja(div, 2)
        assert.equal(2*2, div.firstChild.childNodes.length); // duplo zbog label

      });
      it('test dodajInputPolja() granicni slucajevi', function() {
        var div = document.createElement("div")
        VjezbeAjax.dodajInputPolja(div, 15)
        assert.equal(2*15, div.firstChild.childNodes.length); // duplo zbog label
        VjezbeAjax.dodajInputPolja(div, 0)
        assert.equal(2*0, div.childNodes.length);
      });
      it('test dodajInputPolja() nelegalan broj', function() {
        var div = document.createElement("div")
        VjezbeAjax.dodajInputPolja(div, -1)
        assert.equal(0, div.childNodes.length); // duplo zbog label
        VjezbeAjax.dodajInputPolja(div, 16)
        assert.equal(0, div.childNodes.length);
      });
      it('test iscrtajVjezbe() ok sve', function() {
        var div = document.createElement("div")
        var obj = {"brojVjezbi": 3, "brojZadataka": [1,1,1]}
        VjezbeAjax.iscrtajVjezbe(div, obj)
        assert.equal(3*2, div.childNodes.length); //duplo jer dodaje se div i btn
      });
      it('test iscrtajVjezbe() nelegalan broj vjezbi', function() {
        var div = document.createElement("div")
        var obj = {"brojVjezbi": 0, "brojZadataka": [1,1,1]}
        VjezbeAjax.iscrtajVjezbe(div, obj)
        assert.equal(0, div.childNodes.length); //duplo jer dodaje se div i btn
      });
      it('test iscrtajVjezbe() nelegalan duzina broja zadataka', function() {
        var div = document.createElement("div")
        var obj = {"brojVjezbi": 3, "brojZadataka": [1,1,1,1,1]}
        VjezbeAjax.iscrtajVjezbe(div, obj)
        assert.equal(0, div.childNodes.length); //duplo jer dodaje se div i btn
      });
      it('test iscrtajVjezbe() nelegalan broja zadataka', function() {
        var div = document.createElement("div")
        var obj = {"brojVjezbi": 3, "brojZadataka": [-1,1,1]}
        VjezbeAjax.iscrtajVjezbe(div, obj)
        assert.equal(0, div.childNodes.length); //duplo jer dodaje se div i btn
      });
      it('test iscrtajVjezbe() nelegalan broja zadataka', function() {
        var div = document.createElement("div")
        var obj = {"brojVjezbi": 3, "brojZadataka": ["e","e","e"]}
        VjezbeAjax.iscrtajVjezbe(div, obj)
        assert.equal(0, div.childNodes.length); //duplo jer dodaje se div i btn
      });
      it('test iscrtajZadatke() sve ok', function() {
        var div = document.createElement("div")
       
        VjezbeAjax.iscrtajZadatke(div, 3)
        assert.equal(3, div.childNodes.length); 
      });
      it('test iscrtajZadatke() nelegalan broj', function() {
        var div = document.createElement("div")
       
        VjezbeAjax.iscrtajZadatke(div, 11)
        assert.equal(0, div.childNodes.length); 

        VjezbeAjax.iscrtajZadatke(div, -1)
        assert.equal(0, div.childNodes.length); 

        VjezbeAjax.iscrtajZadatke(div, "e")
        assert.equal(0, div.childNodes.length); 
      });





    });
});