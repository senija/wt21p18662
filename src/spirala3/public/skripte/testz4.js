let assert = chai.assert;
describe('TestoviParser', function() {
    describe('porediRezultate()', function() {
      it('Potpuno isti test', function() {
       let rezultat1 = `{
        "stats": {
            "suites": 2,
            "tests": 2,
            "passes": 2,
            "pending": 0,
            "failures": 0,
            "start": "2021-11-05T15:00:26.343Z",
            "end": "2021-11-05T15:00:26.352Z",
            "duration": 9
        },
        "tests": [
            {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
        ],
        "pending": [],
        "failures": [],
        "passes": [
            {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 3 rows when parameter are 2,3",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
        ]
        }` 
        let actual = TestoviParser.porediRezultate(rezultat1, rezultat1);
        let expected = {promjena: "100%", greske: []}; 
        assert.equal(JSON.stringify(expected), JSON.stringify(actual));
        
      });

      it('Identicni rezultati1', function() {
        let rezultat1 = `{
         "stats": {
             "suites": 2,
             "tests": 2,
             "passes": 2,
             "pending": 0,
             "failures": 0,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test 1",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test 2",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             }
         ],
         "pending": [],
         "failures": [],
         "passes": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test 1",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test 2",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             }
         ]
         }` 

         let rezultat2 = `{
            "stats": {
                "suites": 2,
                "tests": 2,
                "passes": 0,
                "pending": 0,
                "failures": 2,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test 1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test 2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "pending": [],
            "failures": [                
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test 1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test 2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }
            ],
            "passes": []
            }` 
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat2);
         let expected = {promjena: "0%", greske: ["Test 1", "Test 2"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });

       it('Identicni rezultati2', function() {
        let rezultat1 = `{
         "stats": {
             "suites": 3,
             "tests": 3,
             "passes": 1,
             "pending": 0,
             "failures": 2,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test 3",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test 4",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Test 5",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
         ],
         "pending": [],
         "failures": [             
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test 3",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Test 5",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }],
         "passes": [
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test 4",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             }
         ]
         }` 

         let rezultat2 =  `{
            "stats": {
                "suites": 2,
                "tests": 3,
                "passes": 2,
                "pending": 0,
                "failures": 1,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test 3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test 4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "Test 5",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
                
            ],
            "pending": [],
            "failures": [             
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test 4",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
            "passes": [ 
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test 3",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "Test 5",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }

            ]
            }` 
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat2);
         let expected = {promjena: "66.7%", greske: ["Test 4"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });

       it('Razliciti testovi 1 (bez poklapanja)', function() {
        let rezultat1 = `{
         "stats": {
             "suites": 3,
             "tests": 3,
             "passes": 1,
             "pending": 0,
             "failures": 2,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test 4",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test 3",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Test 5",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
         ],
         "pending": [],
         "failures": [             
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test 3",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Test 5",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }],
         "passes": [
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test 4",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             }
         ]
         }` 

         let rezultat2 =  `{
            "stats": {
                "suites": 2,
                "tests": 3,
                "passes": 2,
                "pending": 0,
                "failures": 1,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test 1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test 2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "Test 8",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
                
            ],
            "pending": [],
            "failures": [             
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test 8",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
            "passes": [ 
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test 1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "Test 2",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }

            ]
            }` 
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat2);
         let expected = {promjena: "60%", greske: ["Test 3", "Test 5", "Test 8"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });

       it('Test sortiranja', function() {
        let rezultat1 = `{
         "stats": {
             "suites": 3,
             "tests": 3,
             "passes": 0,
             "pending": 0,
             "failures": 3,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "Test Z",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "Test A",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Test K",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
         ],
         "pending": [],
         "failures": [ {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "Test Z",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "Test A",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
               "title": "should draw 2 columns in row 2 when parameter are 2,3",
               "fullTitle": "Test K",
               "file": null,
               "duration": 0,
               "currentRetry": 0,
               "speed": "fast",
               "err": {}
           }],
         "passes": []
         }` 

         let rezultat2 =  `{
            "stats": {
                "suites": 2,
                "tests": 3,
                "passes": 2,
                "pending": 0,
                "failures": 1,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test A1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test A2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "Test B",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
                
            ],
            "pending": [],
            "failures": [             
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "Test B",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
            "passes": [ 
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "Test A1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "Test A2",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }

            ]
            }` 
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat2);
         let expected = {promjena: "66.7%", greske: ["Test A", "Test K", "Test Z", "Test B"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });

       it('Test razliciti sa preklapanjem (c2)', function() {
        let rezultat1 = `{
         "stats": {
             "suites": 3,
             "tests": 3,
             "passes": 1,
             "pending": 0,
             "failures": 2,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "T1",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "T2",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "T3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
         ],
         "pending": [],
         "failures": [ {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "T1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "T3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }],
         "passes": [
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "T2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
        }]
         }` 

         let rezultat2 =  `{
            "stats": {
                "suites": 2,
                "tests": 3,
                "passes": 2,
                "pending": 0,
                "failures": 1,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "T1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "T2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "T4",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
                
            ],
            "pending": [],
            "failures": [             
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "T2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
            "passes": [ 
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "T1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "T4",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
            ]
            }` 
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat2);
         let expected = {promjena: "50%", greske: ["T3", "T2"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });

       it('Test razliciti sa preklapanjem 3', function() {
        let rezultat1 = `{
         "stats": {
             "suites": 3,
             "tests": 3,
             "passes": 1,
             "pending": 0,
             "failures": 2,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "T1",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "T2",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "T3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
         ],
         "pending": [],
         "failures": [ {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "T1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "T3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }],
         "passes": [
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "T2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
        }]
         }` 

         let rezultat2 =  `{
            "stats": {
                "suites": 2,
                "tests": 4,
                "passes": 2,
                "pending": 0,
                "failures": 4,
                "start": "2021-11-05T15:00:26.343Z",
                "end": "2021-11-05T15:00:26.352Z",
                "duration": 9
            },
            "tests": [
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "T1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "T2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "T3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "T4",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
                
            ],
            "pending": [],
            "failures": [             
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "T2",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                    "title": "should draw 2 columns in row 2 when parameter are 2,3",
                    "fullTitle": "T3",
                    "file": null,
                    "duration": 0,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                }],
            "passes": [ 
                {
                    "title": "should draw 3 rows when parameter are 2,3",
                    "fullTitle": "T1",
                    "file": null,
                    "duration": 1,
                    "currentRetry": 0,
                    "speed": "fast",
                    "err": {}
                },
                {
                   "title": "should draw 2 columns in row 2 when parameter are 2,3",
                   "fullTitle": "T4",
                   "file": null,
                   "duration": 0,
                   "currentRetry": 0,
                   "speed": "fast",
                   "err": {}
               }
            ]
            }` 
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat2);
         let expected = {promjena: "50%", greske: ["T2", "T3"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });  
       
       it('Test nepravilan JSON', function() {
        let rezultat1 = `{
         "stats": {
             suites: 3,
             "tests": 3,
             "passes": 1,
             "pending": 0,
             "failures": 2,
             "start": "2021-11-05T15:00:26.343Z",
             "end": "2021-11-05T15:00:26.352Z",
             "duration": 9
         },
         "tests": [
             {
                 "title": "should draw 3 rows when parameter are 2,3",
                 "fullTitle": "T1",
                 "file": null,
                 "duration": 1,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                 "title": "should draw 2 columns in row 2 when parameter are 2,3",
                 "fullTitle": "T2",
                 "file": null,
                 "duration": 0,
                 "currentRetry": 0,
                 "speed": "fast",
                 "err": {}
             },
             {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "T3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }
         ],
         "pending": [],
         "failures": [ {
                "title": "should draw 3 rows when parameter are 2,3",
                "fullTitle": "T1",
                "file": null,
                "duration": 1,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            },
            {
                "title": "should draw 2 columns in row 2 when parameter are 2,3",
                "fullTitle": "T3",
                "file": null,
                "duration": 0,
                "currentRetry": 0,
                "speed": "fast",
                "err": {}
            }],
         "passes": [
            {
            "title": "should draw 2 columns in row 2 when parameter are 2,3",
            "fullTitle": "T2",
            "file": null,
            "duration": 0,
            "currentRetry": 0,
            "speed": "fast",
            "err": {}
        }]
         }` 

         
         let actual = TestoviParser.porediRezultate(rezultat1, rezultat1);
         let expected = {promjena: "0%", greske: ["Testovi se ne mogu izvršiti"]}; 
         assert.equal(JSON.stringify(expected), JSON.stringify(actual));
         
       });
    });
});
   