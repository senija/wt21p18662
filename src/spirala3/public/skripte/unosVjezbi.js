window.onload = function(){

    let inputBrojVjezbi = document.getElementById("brojVjezbi")
    inputBrojVjezbi.addEventListener('change', event => {
        let brojVjezbi = Number(document.getElementById("brojVjezbi").value);
        if(brojVjezbi < 1 || brojVjezbi > 15) console.log("Neispravan broj vjezbi unesen");
        else if(!Number.isInteger(brojVjezbi)) console.log("Neispravan broj vjezbi unesen");
        else VjezbeAjax.dodajInputPolja(document.getElementById("kontejnerInputa"), brojVjezbi);
        
    })

    let btnPosalji = document.getElementById("btnPosalji")
    console.log(btnPosalji)
    btnPosalji.addEventListener("click", event =>{
        let brojZadataka = [];
        let brojVjezbi = document.getElementById("brojVjezbi").value;
        for(let i=0; i<brojVjezbi; i++){
            let brZadatka = document.getElementById("z" + i).value;
            if(brZadatka === "" || !Number.isInteger(Number(brZadatka)) || brZadatka <0 || brZadatka > 10){
                console.log("Neispravan broj vjezbi unesen");
                return;
            } 
            else brojZadataka.push(Number(brZadatka));
        }
    
        let objekat = {'brojVjezbi': Number(brojVjezbi), 'brojZadataka':brojZadataka};
        VjezbeAjax.posaljiPodatke(objekat, function(err, data){});
    })
    
    
}