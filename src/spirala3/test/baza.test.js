process.env.PORT_SERVER = "5000";
process.env.DATABASE_NAME = "wt2118662test";

const chai = require("chai");
const chaihttp = require("chai-http");
chai.use(chaihttp);
chai.should();
let assert = chai.assert;

const mysql = require("mysql2/promise");
const { db, server } = require("../index.js");

describe("Testovi baze", function () {
  this.timeout(5000);

  this.beforeAll(async () => {
    const connection = await mysql.createConnection({
      host: process.env.DB_HOST || "127.0.0.1",
      user: process.env.DB_USER || "root",
      password: process.env.DB_PASSWORD || "password",
    });
    await connection.query(`CREATE DATABASE IF NOT EXISTS wt2118662test;`);
    console.log("Baza postoji ili kreirana");
    await db.sequelize.sync({ force: true });
    console.log("izvrsi sync");
  });

  this.afterEach(async () => {
    await db.sequelize.sync({ force: true });
  });

  describe("Test POST /student", () => {
    it("Test 1: prvi student", async () => {
      let student = {
        ime: "Senija",
        prezime: "Kaleta",
        index: "18662",
        grupa: "Grupa1",
      };
      let odgovor = await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student));

      assert.equal(odgovor.status, 200);

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 1, "Nije ok broj studenata u bazi");

      let studentBaza = sviStudenti[0];
      assert.equal(studentBaza.ime, student.ime, "Nije ok ime");
      assert.equal(studentBaza.prezime, student.prezime, "Nije ok prezime");
      assert.equal(studentBaza.index, student.index, "Nije ok index");

      let sveGrupe = await db.grupa.findAll();
      assert.equal(sveGrupe.length, 1);
      assert.equal(sveGrupe[0].naziv, student.grupa);
    });
    it("Test 2: Novi student stara grupa", async () => {
      let student1 = {
        ime: "Senija",
        prezime: "Kaleta",
        index: "18662",
        grupa: "Grupa1",
      };
      let student2 = {
        ime: "R",
        prezime: "K",
        index: "18661",
        grupa: "Grupa1",
      };
      let odgovor1 = await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student1));

      assert.equal(odgovor1.status, 200, "Greska sa zahtjevom 1");

      let odgovor2 = await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student2));

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 2);

      let noviStudent = sviStudenti[1];
      assert.equal(noviStudent.ime, student2.ime);
      assert.equal(noviStudent.prezime, student2.prezime);
      assert.equal(noviStudent.index, student2.index);

      let grupe = await db.grupa.findAll();

      assert.equal(grupe.length, 1);
      assert.equal(grupe[0].naziv, student2.grupa);
      assert.equal(grupe[0].naziv, student1.grupa);
    });
    it("Test 3: Dodavanje istog studenta", async () => {
      let student = {
        ime: "Senija",
        prezime: "Kaleta",
        index: "18662",
        grupa: "Grupa1",
      };
      let odgovor1 = await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student));

      assert.equal(odgovor1.status, 200);
      let odgovor2 = await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student));

      assert.equal(odgovor2.status, 200);
      assert.equal(
        JSON.parse(odgovor2.text).status,
        `Student sa indexom ${student.index} vec postoji!`,
        "Status neispravnog jsona"
      );

      let studentiBaza = await db.student.findAll();
      assert.equal(studentiBaza.length, 1);

      let sviStudenti = await db.student.findAll();
      let studentBaza = sviStudenti[0];
      assert.equal(studentBaza.ime, student.ime);
      assert.equal(studentBaza.prezime, student.prezime);
      assert.equal(studentBaza.index, student.index);

      let grupe = await db.grupa.findAll();
      assert.equal(grupe.length, 1);
      assert.equal(grupe[0].naziv, student.grupa);
    });
  });

  describe("PUT /student/:index", () => {
    it("Test 1: Student postoji, validna promjena", async () => {
      let student = {
        ime: "S",
        prezime: "K",
        index: "18662",
        grupa: "Grupa1",
      };
      await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student));

      let grupa = { grupa: "Grupa2" };
      let odgovor1 = await chai
        .request(server)
        .put(`/student/${student.index}`)
        .set("Content-Type", "application/json")
        .send(JSON.stringify(grupa));

      assert.equal(odgovor1.status, 200);
      assert.equal(
        JSON.parse(odgovor1.text).status,
        `Promjenjena grupa studentu ${student.index}`,
        "Status"
      );

      let grupe = await db.grupa.findAll();
      assert.equal(grupe.length, 2);
      assert.equal(grupe[1].naziv, grupa.grupa);
    });
    it("Test 2: Student ne postoji", async () => {
      let grupa = { grupa: "Grupa2" };
      let odgovor1 = await chai
        .request(server)
        .put(`/student/7`)
        .set("Content-Type", "application/json")
        .send(JSON.stringify(grupa));

      assert.equal(odgovor1.status, 200);
      assert.equal(
        JSON.parse(odgovor1.text).status,
        "Student sa indexom 7 ne postoji"
      );

      let grupe = await db.grupa.findAll();
      assert.equal(grupe.length, 0);
    });
    it("Test 3: Student  grupa postoji", async () => {
      let student = {
        ime: "S",
        prezime: "K",
        index: "18662",
        grupa: "Grupa1",
      };
      await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student));

      let student2 = {
        ime: "R",
        prezime: "K",
        index: "18000",
        grupa: "Grupa2",
      };
      await chai
        .request(server)
        .post("/student")
        .set("Content-Type", "application/json")
        .send(JSON.stringify(student2));

      let sveGrupePrije = await db.grupa.findAll();

      let grupa = { grupa: "Grupa2" };
      let odgovor1 = await chai
        .request(server)
        .put(`/student/${student.index}`)
        .set("Content-Type", "application/json")
        .send(JSON.stringify(grupa));

      assert.equal(odgovor1.status, 200);

      let grupaPromjena = await db.grupa.findOne({
        where: { naziv: grupa.grupa },
      });
      let studentiGrupe = await db.student.findAll({
        where: { grupaId: grupaPromjena.dataValues.id },
      });
      assert.equal(studentiGrupe.length, 2);
      const trazenaGrupa = await db.grupa.findOne({
        where: { id: studentiGrupe[0].dataValues.grupaId },
      });
      assert.equal(trazenaGrupa.naziv, grupa.grupa);
    });
  });

  describe("Test POST /batch/student", () => {
    it("Test 1: Dodavanje studenta preko batch", async () => {
      let csv = `S,K,18662,Grupa1`;
      let odgovor = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv);

      assert.equal(odgovor.status, 200);

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 1);

      let studentBaza = sviStudenti[0];
      assert.equal(studentBaza.ime, "S");
      assert.equal(studentBaza.prezime, "K");
      assert.equal(studentBaza.index, "18662");

      let sveGrupe = await db.grupa.findAll();
      assert.equal(sveGrupe.length, 1);
      assert.equal(sveGrupe[0].naziv, "Grupa1");
    });
    it("Test 2: Dodavanje vise studenta preko batch", async () => {
      let csv =
        "S,K,17777,Grupa1\nS,K,16666,Grupa2\nS,K,18662,Grupa1\nS,K,19999,Grupa3\n";
      let odgovor = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv);
      assert.equal(odgovor.status, 200);

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 4);

      let sveGrupe = await db.grupa.findAll();
      assert.equal(sveGrupe.length, 3);
    });
    it("Test 3: isti student preko batch", async () => {
      let csv = "S,K,17777,Grupa1\n" + "S,K,17777,Grupa1";
      let odgovor = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv);

      assert.equal(odgovor.status, 200);

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 1);

      let sveGrupe = await db.grupa.findAll();
      assert.equal(sveGrupe.length, 1);
    });
    it("Test 4: dodavanje postojecih", async () => {
      let csv1 = "S,K,17777,Grupa1\n" + "S,K,16666,Grupa2\n";
      let odgovor1 = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv1);

      assert.equal(odgovor1.status, 200);

      let csv2 =
        "S,K,17777,Grupa1\n" +
        "S,K,16666,Grupa2\n" +
        "S,K,18662,Grupa1\n" +
        "S,K,19999,Grupa3\n";
      let odgovor2 = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv2);

      assert.equal(odgovor2.status, 200);
      assert.equal(
        JSON.parse(odgovor2.text).status,
        `Dodano 2 studenata, a studenti 17777,16666 vec postoje!`,
        "Status"
      );

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 4);

      let sveGrupe = await db.grupa.findAll();
      assert.equal(sveGrupe.length, 3);
    });
    it("Test 5: dodavanje postojecih 2", async () => {
      let csv1 =
        "S,K,1,Grupa1\n" +
        "S,K,2,Grupa2\n" +
        "S,K,3,Grupa1\n" +
        "S,K,4,Grupa3\n";
      let odgovor1 = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv1);

      assert.equal(odgovor1.status, 200);

      let csv2 =
        "A,K,17777,Grupa1\n" +
        "A,K,1,Grupa2\n" +
        "A,K,16666,Grupa1\n" +
        "A,K,2,Grupa3\n" +
        "A,K,3,Grupa2\n" +
        "A,K,18662,Grupa1\n" +
        "A,K,19999,Grupa3\n" +
        "A,K,4,Grupa3\n";
      let odgovor2 = await chai
        .request(server)
        .post("/batch/student")
        .set("Content-Type", "text/plain")
        .send(csv2);

      assert.equal(odgovor2.status, 200);

      let sviStudenti = await db.student.findAll();
      assert.equal(sviStudenti.length, 8);
    });
  });
});
