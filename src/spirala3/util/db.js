const Sequelize = require("sequelize");
const { QueryTypes } = require('sequelize');
const imeBaze = process.env.DATABASE_NAME || "wt2118662"
const sequelize = new Sequelize(imeBaze, "root", "password", {
    host: "127.0.0.1",
    dialect: "mysql"
});

const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize; // za sta je ovo 

db.vjezba = require('../models/Vjezba')(sequelize); //spajanje i guesss
db.student = require('../models/Student')(sequelize);
db.grupa = require('../models/Grupa')(sequelize);
db.zadatak = require('../models/Zadatak')(sequelize);

db.grupa.hasMany(db.student,{as:'studentiGrupe'});
db.vjezba.hasMany(db.zadatak,{as:'zadaciVjezbe'})

module.exports=db;


